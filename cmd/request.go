// Copyright © 2018 Heiko Reese <heiko.reese@kit.edu>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"log"

	"crypto/rsa"
	"crypto/x509/pkix"
	"io/ioutil"
	"os"
	"strings"

	"crypto/x509"
	"encoding/pem"
	"net"
	"net/url"
	"path/filepath"

	"bytes"
	"text/template"

	"git.scc.kit.edu/KIT-CA/dfnpki"
	"github.com/kennygrant/sanitize"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

type sanGenerator struct {
	Kind   int
	Values []string
}

// requestCmd represents the request command
var requestCmd = &cobra.Command{
	Use:   "request",
	Short: "Request a new certificate and retrieve the pdf application form",
	// Long: ``,
	TraverseChildren: true,
	Run: func(cmd *cobra.Command, args []string) {
		var (
			err          error
			pkey         *rsa.PrivateKey
			commonName   string
			profile      string
			basefilename string
			outputdir    string
		)
		_ = viper.BindPFlags(cmd.PersistentFlags())
		// write config values into cmdArguments
		_ = viper.Unmarshal(&cmdArguments)

		// check ca name
		if !viper.IsSet("ca") {
			log.Fatal("Please specify a valid CA")
		}

		// check cn
		if viper.GetString("cn") == "" {
			log.Fatal("Please specify a common name (»CN«) via --cn")
		}

		// clean up CommonName
		cnCleanUp := strings.NewReplacer("Ä", "Ae", "Ö", "Oe", "Ü", "Ue", "ä", "ae", "ö", "oe", "ü", "ue", "ß", "ss")
		commonName = viper.GetString("cn")
		commonNameCleaned := cnCleanUp.Replace(commonName)
		if commonNameCleaned != commonName {
			log.Printf("Transcribed illegal characters in cn »%s«: »%s«", commonName, commonNameCleaned)
			viper.Set("cn", commonNameCleaned)
			commonName = commonNameCleaned
		}

		// check profile
		profile = viper.GetString("profile")
		dfnrole, exists := dfnpki.DFNPublicRoles[profile]
		if !exists {
			log.Fatal("Unknown profile ", profile)
		}

		// set default outpath and filename
		cmdArguments.CommonNameSanitized = sanitize.Name(commonName)
		basefilename = sanitize.Name(commonName)

		// generate output directory name
		if !viper.IsSet("outdir") {
			outputdir = basefilename + "_" + cmdArguments.TimeStamp
			viper.Set("outdir", outputdir)
		} else {
			directoryTemplate, err := template.New("outdir").Parse(viper.GetString("outdir"))
			if err != nil {
				// last chance fallback…
				outputdir = viper.GetString("outdir")
			}
			outdirwriter := bytes.Buffer{}
			err = directoryTemplate.Execute(&outdirwriter, cmdArguments)
			if err != nil {
				// last chance fallback…
				outputdir = viper.GetString("outdir")
			}
			outputdir = outdirwriter.String()
		}
		// create output path
		if viper.GetBool("dry-run") == false {
			err = os.MkdirAll(outputdir, 0700)
			if err != nil {
				log.Fatalf("Unable to create output directory %s: %s", outputdir, err)
			}
		}
		log.Printf("Output directory is »%s«", outputdir)

		// create http client
		httpclient, err := dfnpki.GetPublicHTTPClient()
		if err != nil {
			log.Fatal(err)
		}
		client := dfnpki.NewSoapClient(httpclient, dfnpki.GeneratePublicURL(viper.GetString("ca")))

		// generate PIN if needed
		if viper.GetString("PIN") == "" {
			viper.Set("PIN", dfnpki.RandomPIN())
			log.Printf("Generated PIN: »%s«", viper.GetString("PIN"))
		}
		filename := filepath.Join(outputdir, basefilename+".PIN.txt")
		if viper.GetBool("dry-run") == false {
			err = ioutil.WriteFile(filename, []byte(viper.GetString("PIN")), 0644)
			if err != nil {
				log.Fatalf("Unable to write PIN file %s: %s", filename, err)
			}
		}
		log.Printf("Wrote PIN to »%s«", filename)

		// handle private key
		keyfilename := viper.GetString("keyfile")
		if keyfilename != "" {
			// read keyfile
			keybytes, err := ioutil.ReadFile(keyfilename)
			if err != nil {
				log.Fatalf("Unable to read secret key from file %s: %s", keyfilename, err)
			}
			pkey, err = dfnpki.ParsePrivateKey(keybytes, []byte(viper.GetString("keypass")))
			if err != nil {
				log.Fatalf("Unable to parse secret key from file %s: %s", keyfilename, err)
			}
			log.Printf("Read private key with %d bytes from %s", pkey.N.BitLen(), keyfilename)
		} else {
			// generate key
			pkey, err = dfnpki.NewPrivateKey(viper.GetInt("keysize"))
			if err != nil {
				log.Fatalf("Unable to generate secret key: %s", err)
			} else {
				log.Printf("Private rsa key with %d bytes generated", pkey.N.BitLen())
			}
			// save key as pkcs1/der
			pkcs1key := x509.MarshalPKCS1PrivateKey(pkey)
			filename := filepath.Join(outputdir, basefilename+".key.der")
			if viper.GetBool("dry-run") == false {
				err = ioutil.WriteFile(filename, pkcs1key, 0600)
				if err != nil {
					log.Fatalf("Unable to write private to »%s« as PKCS1/DER: %s", filename, err)
				}
			}
			log.Printf("Wrote private key to »%s« (format: PKCS1/DER)", filename)

			// save key as pkcs1/pem
			pemkey := pem.EncodeToMemory(&pem.Block{
				Bytes: pkcs1key,
				Type:  "RSA PRIVATE KEY",
			})
			filename = filepath.Join(outputdir, basefilename+".key.pem")
			if viper.GetBool("dry-run") == false {
				err = ioutil.WriteFile(filename, pemkey, 0600)
				if err != nil {
					log.Fatalf("Unable to write private to »%s« as PKCS1/PEM: %s", filename, err)
				}
			}
			log.Printf("Wrote private key to »%s« (format: PKCS1/PEM)", filename)

		}

		// prepare certificate request
		request := &x509.CertificateRequest{
			Subject: pkix.Name{
				CommonName: commonName,
			},
			SignatureAlgorithm: x509.SHA256WithRSA,
		}
		// OU
		if viper.IsSet("ou") {
			request.Subject.OrganizationalUnit = viper.GetStringSlice("ou")
		}
		// O
		if viper.GetString("o") != "" {
			request.Subject.Organization = []string{viper.GetString("o")}
		} else {
			log.Fatal("Please specify Organization (»O«)")
		}
		// L
		if viper.GetString("locality") != "" {
			request.Subject.Locality = []string{viper.GetString("locality")}
		}
		// ST
		if viper.GetString("state") != "" {
			request.Subject.Province = []string{viper.GetString("state")}
		}
		// C
		if viper.GetString("country") != "" {
			request.Subject.Country = []string{viper.GetString("country")}
		} else {
			log.Fatal("Please specify Country (»C«)")
		}
		// DNS SAN
		if viper.IsSet("dns") {
			request.DNSNames = viper.GetStringSlice("dns")
		}
		// email SAN
		if viper.IsSet("email") {
			request.EmailAddresses = viper.GetStringSlice("email")
		}
		// IP SAN
		if viper.IsSet("ip") {
			if ip, ok := viper.Get("ip").([]net.IP); ok {
				request.IPAddresses = ip
			}
		}
		// add URL SANS
		if viper.IsSet("uri") {
			if urls, ok := viper.Get("uri").([]*url.URL); ok {
				request.URIs = urls
			}
		}

		// generate certificate request
		csr, err := dfnpki.GenerateRequest(pkey, request)
		if err != nil {
			log.Fatal("Unable to generate pkcs10 certificate request: ", err)
		}
		filename = filepath.Join(outputdir, basefilename+".req")
		if viper.GetBool("dry-run") == false {
			err = ioutil.WriteFile(filename, []byte(csr), 0644)
			if err != nil {
				log.Fatalf("Unable to write request file %s: %s", filename, err)
			}
		}
		log.Printf("Wrote request to »%s« (format: PKCS10)", filename)

		// generate compatible argument for SAN addition via API
		var SubjectAlternativeNames []string
		for _, sanGenerator := range []sanGenerator{
			{
				Kind:   dfnpki.SANEmail,
				Values: viper.GetStringSlice("email"),
			},
			{
				Kind:   dfnpki.SANDNS,
				Values: viper.GetStringSlice("dns"),
			},
			{
				Kind:   dfnpki.URI,
				Values: viper.GetStringSlice("uri"),
			},
			{
				Kind:   dfnpki.IP,
				Values: viper.GetStringSlice("ip"),
			},
		} {
			for _, value := range sanGenerator.Values {
				SubjectAlternativeNames = append(SubjectAlternativeNames, dfnpki.SAN(sanGenerator.Kind, value))
			}
		}
		// fix requester date for profile User
		if profile == "User" {
			if viper.GetString("RequesterName") == "" {
				viper.Set("RequesterName", commonName)
			}
			if viper.GetString("RequesterEmail") == "" {
				emailArgs := viper.GetStringSlice("email")
				if len(emailArgs) > 0 {
					viper.Set("RequesterEmail", emailArgs[0])
				} else {
					log.Fatal("No email address set")
				}
			}
		}
		// prepare request
		requestData := dfnpki.NewRequestData{
			RaId:       viper.GetInt("ra-id"),
			Pkcs10:     csr,
			AltNames:   SubjectAlternativeNames,
			Role:       dfnrole,
			PIN:        viper.GetString("pin"),
			AddName:    viper.GetString("RequesterName"),
			AddEMail:   viper.GetString("RequesterEmail"),
			AddOrgUnit: viper.GetString("RequesterOU"),
			Publish:    viper.GetBool("Publish"),
		}

		// make request
		var requestID string
		if viper.GetBool("dry-run") == false {
			requestID, err = dfnpki.NewRequest(client, csr, requestData)
			if err != nil {
				log.Fatal("Error sending request to api endpoint: ", err)
			}
		} else {
			requestID = "DRYRUN"
		}
		log.Printf("New request with ID %s created", requestID)

		// save request id
		filename = filepath.Join(outputdir, basefilename+".request-id.txt")
		if viper.GetBool("dry-run") == false {
			err = ioutil.WriteFile(filename, []byte(requestID+"\n"), 0644)
			if err != nil {
				log.Fatalf("Unable to write request file %s: %s", filename, err)
			}
		}
		log.Printf("Request ID written to »%s«", filename)

		// fetch PDF
		if viper.GetBool("SkipPDF") == false {
			pdf, err := dfnpki.GetRequestPrintout(client, viper.GetInt("ra-id"), requestID, viper.GetString("pin"))
			if err != nil {
				log.Fatalf("Unable to retrieve PDF: %s", err)
			}
			filename = filepath.Join(outputdir, basefilename+".request.pdf")
			if viper.GetBool("dry-run") == false {
				err = ioutil.WriteFile(filename, pdf, 0644)
				if err != nil {
					log.Fatalf("Unable to write PDF file »%s«: %s", filename, err)
				}
			}
			log.Printf("PDF request saved to »%s«", filename)
		}
	},
}

func init() {
	// just logging, no prefixes
	log.SetFlags(0)

	rootCmd.AddCommand(requestCmd)

	// flags and configuration settings
	requestCmd.PersistentFlags().SortFlags = false
	requestCmd.PersistentFlags().StringVar(&cmdArguments.Profile, "profile", "Web Server", "Certificate profile")
	requestCmd.PersistentFlags().StringVar(&cmdArguments.CommonName, "cn", "", "Set »CommonName« (common name) part of distinguished name")
	requestCmd.PersistentFlags().StringSliceVar(&cmdArguments.OU, "ou", nil, "Set »OU« (organizational unit) part(s) of distinguished name")
	requestCmd.PersistentFlags().StringVar(&cmdArguments.Organisation, "o", "", "Set »O« (organization) part of distinguished name")
	requestCmd.PersistentFlags().StringVar(&cmdArguments.Locality, "locality", "", "Set »L« (locality) part of distinguished name")
	requestCmd.PersistentFlags().StringVar(&cmdArguments.State, "state", "", "Set »ST« (state)  part of distinguished name")
	requestCmd.PersistentFlags().StringVar(&cmdArguments.Country, "country", "", "Set »C« (country) part of distinguished name")
	requestCmd.PersistentFlags().StringSliceVar(&cmdArguments.DNS, "dns", nil, "Add »DNS« (hostname or domain name) Subject Alternative Name(s)")
	requestCmd.PersistentFlags().StringSliceVar(&cmdArguments.IP, "ip", nil, "Add »IP« (ip address) Subject Alternative Name(s)")
	requestCmd.PersistentFlags().StringSliceVar(&cmdArguments.URI, "uri", nil, "Add »URI« Subject Alternative Name(s)")
	requestCmd.PersistentFlags().StringSliceVar(&cmdArguments.Email, "email", nil, "Add »email« Subject Alternative Name(s)")
	requestCmd.PersistentFlags().StringVar(&cmdArguments.RequesterName, "RequesterName", "", "Name of requester (»Beantrager«); set to CommonName for personal certificate")
	requestCmd.PersistentFlags().StringVar(&cmdArguments.RequesterEmail, "RequesterEmail", "", "E-Mail of requester (»Beantrager«)")
	requestCmd.PersistentFlags().StringVar(&cmdArguments.RequesterOU, "RequesterOU", "", "Organisational Unit of requester (»Beantrager«)")
	requestCmd.PersistentFlags().BoolVar(&cmdArguments.Publish, "publish", true, "Publish certificate")
	requestCmd.PersistentFlags().StringVar(&cmdArguments.PIN, "pin", "", "PIN for revocation and retrieval of unpublished certificates")
	requestCmd.PersistentFlags().StringVarP(&cmdArguments.Keyfile, "keyfile", "k", "", "Read key from filename if set; autogenerated otherwise")
	_ = requestCmd.MarkFlagFilename("keyfile", "")
	requestCmd.PersistentFlags().StringVarP(&cmdArguments.Keypass, "keypass", "p", "", "Password if secret key is encrypted")
	requestCmd.PersistentFlags().IntVar(&cmdArguments.Keysize, "keysize", 4096, "Size of secret key in bits (only used if --keyfile is not set; minimum size 2048 bits)")
	requestCmd.PersistentFlags().StringVarP(&cmdArguments.Outdir, "outdir", "o", "{{-.CommonNameSanitized}}_{{-.TimeStamp}}", "Directory for all output files")
	requestCmd.PersistentFlags().BoolVarP(&cmdArguments.DryRun, "dry-run", "n", false, "Only show request data, don't execute anything")
	requestCmd.PersistentFlags().BoolVarP(&cmdArguments.SkipPDF, "skip-pdf", "s", false, "Don't fetch pdf after request")
}
