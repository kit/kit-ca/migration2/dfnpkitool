// Copyright © 2018 Heiko Reese
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

package cmd

import (
	"fmt"
	"os"

	"path/filepath"

	"log"

	"time"

	"github.com/mitchellh/go-homedir"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

const configsubdir = ".config"

var cfgFile string

// rootCmd represents the base command when called without any subcommands
var (
	rootCmd = &cobra.Command{
		Use:   "dfnpkitool",
		Short: "cli interface to the dfn-pki soap api.",
		// Long: ``,
		// Uncomment the following line if your bare application
		// has an action associated with it:
		//Run: func(cmd *cobra.Command, args []string) {
		//},
		TraverseChildren: true,
	}
	// see also: Zertifizierungsrichtlinie der DFN-PKI-Sicherheitsniveau Global, Version:3.8, (OID): 1.3.6.1.4.1.22177.300.1.1.4.3.8
	cmdArguments = struct {
		CAName              string
		RAId                int
		Profile             string
		CommonName          string
		Organisation        string
		OU                  []string
		Locality            string
		State               string
		Country             string
		DNS                 []string
		IP                  []string
		URI                 []string
		Email               []string
		RequesterName       string
		RequesterEmail      string
		RequesterOU         string
		Keyfile             string
		Keypass             string
		Keysize             int
		PIN                 string
		Outdir              string
		SkipPDF             bool
		Publish             bool
		DryRun              bool
		TimeStamp           string
		CommonNameSanitized string
	}{
		TimeStamp: time.Now().Format("2006-01-02T15_04_05"),
	}
)

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	rootCmd.Flags().SortFlags = false
	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.config/dfnpkitool.{toml,yaml,json})")
	rootCmd.PersistentFlags().StringVarP(&cmdArguments.CAName, "ca", "C", "", "Certification Authority identifier")
	rootCmd.PersistentFlags().IntVarP(&cmdArguments.RAId, "ra-id", "R", 0, "Sub-RA identifier (usually 0 for main RA)")

	viper.BindPFlags(rootCmd.PersistentFlags())
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := homedir.Dir()
		if err != nil {
			log.Fatal(err)
		}

		// Search config in home directory with name "dfnpkitool" (without extension).
		viper.SetConfigName("dfnpkitool")
		viper.AddConfigPath(filepath.Join(home, configsubdir))
		viper.AddConfigPath(".")
	}

	// If a config file is found, read it in.
	err := viper.ReadInConfig()
	if err != nil {
		//log.Println("Using config file:", viper.ConfigFileUsed())
		log.Println("Error reading config file: ", err)
	}
}
