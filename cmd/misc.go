package cmd

import (
	"errors"
	"log"
	"net"
	"net/url"
	"strings"
)

func IPSliceToStrings(ip interface{}) []string {
	if v, ok := ip.([]net.IP); ok {
		iplist := make([]string, 0, len(v))
		for _, ip := range v {
			iplist = append(iplist, ip.String())
		}
		return iplist
	} else {
		log.Fatal(ok)
	}
	log.Fatal("Unable to convert ip addresses")
	return nil
}

// URLSlice stores urls from cmdline arguments
type URLSlice []*url.URL

func (u *URLSlice) String() string {
	urls := make([]string, 0, len(*u))
	for _, uri := range *u {
		urls = append(urls, uri.String())
	}
	return strings.Join(urls, ",")
}

func (u *URLSlice) Set(rawurl string) error {
	for _, singleurl := range strings.Split(rawurl, ",") {
		uri, err := url.Parse(singleurl)
		if err != nil {
			return err
		}
		*u = append(*u, uri)
	}
	return nil
}

func (u *URLSlice) Type() string {
	return "URLSlice"
}

// IPSlice stores ip addresses from cmdline arguments
type IPSLice []*net.IP

func (i *IPSLice) String() string {
	ips := make([]string, 0, len(*i))
	for _, ip := range *i {
		ips = append(ips, ip.String())
	}
	return strings.Join(ips, ",")
}

func (i *IPSLice) Set(ipstring string) error {
	for _, singleip := range strings.Split(ipstring, ",") {
		ip := net.ParseIP(singleip)
		if ip == nil {
			return errors.New("Unable to parse IP " + singleip)
		}
		*i = append(*i, &ip)
	}
	return nil
}

func (i *IPSLice) Type() string {
	return "IPSlice"
}
