// Copyright © 2018 Heiko Reese <heiko.reese@kit.edu>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"bytes"
	"errors"
	"io/ioutil"
	"log"
	"path/filepath"
	"strings"
	"text/template"

	"git.scc.kit.edu/KIT-CA/dfnpki"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var (
	getPdfData struct {
		RequestID   string
		RequestFile string
	}
)

func ReadFirstChunkFromFile(filename string) (string, error) {
	content, err := ioutil.ReadFile(filename)
	if err != nil {
		return "", err
	}
	tokens := strings.Fields(string(content))
	if len(tokens) < 1 {
		return "", errors.New("Unable to find useful data in" + filename)
	}
	return strings.TrimSpace(tokens[0]), nil
}

// getpdfCmd represents the getpdf command
var getpdfCmd = &cobra.Command{
	Use:   "getpdf",
	Short: "Download pdf document for certificate request",
	// Long: ``,
	Run: func(cmd *cobra.Command, args []string) {
		var (
			filename string
		)
		viper.BindPFlags(cmd.PersistentFlags())
		viper.Unmarshal(&getPdfData)

	REQID:
		switch {
		// handle --requestid
		case viper.IsSet("requestid"):
			getPdfData.RequestID = viper.GetString("requestid")
		// handle --requestidfile
		case viper.IsSet("requestidfile"):
			chunk, err := ReadFirstChunkFromFile(viper.GetString("requestidfile"))
			if err != nil {
				log.Fatalf("Unable to read request if from %s", viper.GetString("requestidfile"))
			}
			getPdfData.RequestID = chunk
		// try to find a request id file
		default:
			matches, err := filepath.Glob(`*.request-id.txt`)
			if err != nil || len(matches) == 0 {
				log.Fatal("Please specify a request id")
			}
			for _, file := range matches {
				chunk, err := ReadFirstChunkFromFile(file)
				if err == nil {
					getPdfData.RequestID = chunk
					break REQID
				}
			}
			log.Fatal("Please specify a request id")
		}

		// create http client
		httpclient, err := dfnpki.GetPublicHTTPClient()
		if err != nil {
			log.Fatal(err)
		}
		client := dfnpki.NewSoapClient(httpclient, dfnpki.GeneratePublicURL(viper.GetString("ca")))

		// generate output filename
		filenameTemplate, err := template.New("outfile").Parse(viper.GetString("outfile"))
		if err != nil {
			// last chance fallback…
			filename = viper.GetString("outfile")
		}
		outfilewriter := bytes.Buffer{}
		err = filenameTemplate.Execute(&outfilewriter, getPdfData)
		if err != nil {
			// last chance fallback…
			filename = viper.GetString("outfile")
		}

		pdf, err := dfnpki.GetRequestPrintout(client, viper.GetInt("ra-id"), getPdfData.RequestID, viper.GetString("pin"))
		if err != nil {
			log.Fatalf("Unable to retrieve PDF: %s", err)
		}
		err = ioutil.WriteFile(filename, pdf, 0644)
		if err != nil {
			log.Fatalf("Unable to write PDF file »%s«: %s", filename, err)
		}
		log.Printf("PDF request saved to »%s«", filename)
	},
}

func init() {
	rootCmd.AddCommand(getpdfCmd)

	getpdfCmd.PersistentFlags().SortFlags = false
	getpdfCmd.PersistentFlags().StringVar(&getPdfData.RequestID, "requestid", "", "Request id (»Seriennummer«)")
	getpdfCmd.PersistentFlags().StringVar(&getPdfData.RequestFile, "requestidfile", "", "File containing the request id (»Seriennummer«) on the first line")
	getpdfCmd.MarkFlagFilename("requestidfile", "")
	getpdfCmd.PersistentFlags().StringVarP(&cmdArguments.Outdir, "outfile", "o", "{{- .RequestID }}.pdf", "Directory for all output files")
	getpdfCmd.PersistentFlags().StringVar(&cmdArguments.PIN, "pin", "", "Request PIN")
	getpdfCmd.MarkFlagRequired("pin")
}
