// Copyright © 2020 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"encoding/json"
	"fmt"
	"log"

	"git.scc.kit.edu/KIT-CA/dfnpki"
	"github.com/spf13/viper"

	"github.com/spf13/cobra"
)

var (
	caInfoArgs struct {
		JSON      bool
		ShowCerts bool
	}
)

// caInfoCmd represents the caInfo command
var caInfoCmd = &cobra.Command{
	Use:   "caInfo",
	Short: "Show public info about a CA: allowed DN-prefixes, public roles and the trust chain",
	Run: func(cmd *cobra.Command, args []string) {
		var (
			printJSON, showCerts bool
		)
		_ = viper.BindPFlags(cmd.PersistentFlags())
		//_ = viper.Unmarshal(&cmdArguments)
		_ = viper.Unmarshal(&caInfoArgs)

		// handle --showcerts
		if viper.IsSet("showcerts") {
			showCerts = true
		}

		// handle --json
		if viper.IsSet("json") {
			printJSON = true
		}

		// create http client
		httpclient, err := dfnpki.GetPublicHTTPClient()
		if err != nil {
			log.Fatal(err)
		}
		client := dfnpki.NewSoapClient(httpclient, dfnpki.GeneratePublicURL(viper.GetString("ca")))

		// get CA info
		cainfo, err := dfnpki.GetCAInfoData(client, 0)
		if err != nil {
			log.Fatal(err)
		}

		if printJSON == true {
			jsonbytes, err := json.MarshalIndent(cainfo, "  ", "")
			if err != nil {
				log.Fatalf("Unable to generate json: %s", err)
			}
			fmt.Println(string(jsonbytes))
		} else {
			fmt.Println("\n# Valid DN prefixes\n")
			for _, prefix := range cainfo.DNPrefixes {
				fmt.Printf("* »%s«\n", prefix)
			}
			fmt.Println("\n# Public roles\n")
			for _, role := range cainfo.Roles {
				fmt.Printf("* »%s«\n", role)
			}
			if showCerts {
				fmt.Println("\n# Certificate chain\n")
				for _, cert := range cainfo.CAChain {
					fmt.Println(cert)
				}
			}
		}
	},
}

func init() {
	rootCmd.AddCommand(caInfoCmd)

	// flags and configuration settings
	caInfoCmd.PersistentFlags().SortFlags = false
	caInfoCmd.PersistentFlags().BoolVar(&caInfoArgs.ShowCerts, "showcerts", false, "Print chain on textual output")
	caInfoCmd.PersistentFlags().BoolVar(&caInfoArgs.JSON, "json", false, "Print json instead of text")
}
