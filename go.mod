module git.scc.kit.edu/KIT-CA/dfnpkitool

go 1.14

require (
	git.scc.kit.edu/KIT-CA/dfnpki v0.0.0-20200812084546-4f5c85c655aa
	github.com/kennygrant/sanitize v1.2.4
	github.com/mitchellh/go-homedir v1.1.0
	github.com/spf13/cobra v1.0.0
	github.com/spf13/viper v1.7.1
)
